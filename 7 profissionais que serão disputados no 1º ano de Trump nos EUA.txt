Donald Trump:  presidente eleito faz discurso em dezembro de 2016 (Mark Wallheiser / Stringer/Getty Images)
São Paulo – Professores e profissionais das áreas de saúde devem ter mais facilidade para encontrar emprego no primeiro ano do governo Trump nos Estados Unidos, segundo levantamento realizado pelo Glassdoor.
Dos sete cargos em alta, quatro são da área de saúde. O crescimento da expectativa de vida e a incerteza em relação em às políticas para planos de saúde estão entre os motivos para que esses profissionais sejam mais disputados em 2017.
Professores
Faltam professores de matemática e ciências e por isso, segundo o Glassdoor, a profissão é uma das mais quentes para este ano nos Estados Unidos. De acordo com o site, 75% dos 200 distritos da Califórnia estão com dificuldades para encontrar professores.
Muitos alunos por classe, testes padronizados e baixos salários explicam a falta de interesse. No Brasil, professores também sofrem com queda salarial, segundo estudo exclusivo feito pela Fipe e publicado no fim do ano passado por Exame.com.
Fisioterapeutas
O fisioterapeuta é um dos profissionais que serão mais demandados até a próxima década, segundo previu U.S. Bureau of Labor Statistics. Atendimentos em escritórios, hospitais e também domésticos devem crescer tanto para fisioterapeutas, como também para terapeutas ocupacionais e assistentes.
Odontólogo especializado em limpeza dental
Além do crescimento da demanda por profissionais focados em limpeza dental, o Glassdoor também aponta para a expansão das atividades. Procedimentos cosméticos, tais como branqueamento também estão em alta para 2017, além da importante atuação desses profissionais no diagnóstico precoce de sérios problemas de saúde bucal.
Enfermeiros
A incerteza em relação ao futuro do programa Obamacare que permitiu acesso de muitos estadunidenses a planos de saúde, e os altos custos dos serviços de saúde podem fazer com que muita gente busque menos o auxílio médio e mais a ajuda de profissionais da área de enfermagem.
Cuidadores
O aumento da expectativa de vida deve ser responsável pelo surgimento de 160 mil novos postos de trabalho até o ano que vem para estes profissionais. O ponto negativo, de acordo com o Glassdoor, é o salário que gira em torno do mínimo. Mas os cursos em alta dos lares específicos para idosos podem fazer subir os rendimentos mensais.
Analistas de mercado
Profissionais que reúnem dados, analisam tendências para prever quais serão os serviços e produtos com mais demanda são essenciais para a estratégia de marketing das empresas. De acordo com o Glassdoor nos próximos anos devem surgir mais de 100 mil novos empregos na área.
Engenheiros de petróleo
Muitos podem estranhar este profissional na lista, em tempos de baixa no preço de combustíveis e de indústria de petróleo em baixa. Mas, segundo o Glassdoor, iniciativas da OPEC de corte de produção para elevar preços e o boom de perfuração de solo por (faturamento hidráulico) que tem ocorrido nos Estados Unidos vão turbinar a demanda por esses profissionais. E nesse caso, salários podem chegar a 100 mil dólares por ano
