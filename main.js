var pg = require('pg');
var request = require('request');
var cheerio = require('cheerio');
var events = require('events');
var fs = require('fs');
var sync = require('sync');

var config = {
    user: 'postgres', //env var: PGUSER
    database: 'findme', //env var: PGDATABASE
    password: 'local', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5432, //env var: PGPORT
    max: 30, // max number of clients in the pool
    idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};


var pool = new pg.Pool(config);



request('http://exame.abril.com.br/noticias-sobre/profissoes/', function(err, resp, body){
    $ = cheerio.load(body);
    titles = $('.list-item-title a');
    $(titles).each(function(i, title){
        request($(title).attr('href'), function(err, resp, body){
            $ = cheerio.load(body);
            var title = $('.article-title').text();
            var img = $('.article-content .featured-image img').attr('data-src');
            var subtitle = $('.article-subtitle').text();
            paragraphs = $('.article-content p, .article-content h3');
            var text= "";
            $(paragraphs).each(function(i, paragraph){
                text += $(paragraph).text() + "\n";
            });
            insertNew({title: title, text: text, subtitle: subtitle, image: img});
        });
    });
});


function insertNew(data){
    pool.connect(function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query('INSERT INTO news(title,subtitle,text,image,created) VALUES($1,$2,$3,$4,now())', [data.title, data.subtitle, data.text, data.image], function(err, resp, body){
            console.log(err);
            console.log(resp);
            done();
        });


    });
}

